<?php

include_once('./user.php');
include_once('../../config/database.php');


$con = new DBConnection();
$db = $con->connectDB();
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$user = new UserApi($db);

$request_method = $_SERVER['REQUEST_METHOD'];

function handleRequest($request_method, $request, $user)
    {
        
        
        switch($request_method){
            case 'GET':
                if($request[0]== ''){
                    $user->getAllUsers();    
                }
                else
                {
                    $id = $request[0];
                    $user->getUserById($id);    
                }
                break;
            case 'POST':
                if($request[0]== 'login'){
                    $user->login();    
                } else if($request[0]== 'register') {
                    $user->register();
                } else if($request[0]== 'update')
                {
                    $id = $request[1];
                    if($id != ""){
                        $user->updateUserById($id);
                    }else
                    {
                        echo('NO USER FOUND WITH SUCH AN ID');
                    }
                } else if($request[0]=='delete'){
                    $id = $request[1];
                    if($id != ""){
                        $user->deleteUserById($id);
                    }else
                    {
                        echo('NO USER FOUND WITH SUCH AN ID');
                    }
                }
                break;
            
            default:
                break;
        }

    }

    handleRequest($request_method,$request,$user)
?>