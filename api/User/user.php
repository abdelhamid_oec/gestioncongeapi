<?php
include_once('../../library/vendor/autoload.php');

use Firebase\JWT\JWT;

use function PHPSTORM_META\type;

class UserApi {
    public $db;

    function __construct($db)
    {
        $this->db= $db;
    }

    function getAllUsers(){
        if(isset($_GET)){
            $statement = $this->db->prepare('SELECT * FROM User');
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            $json= array(
                'error'=>false,
                'data'=>$result,
                'status'=>200
            );
            echo json_encode($json);
        }
    }

    function getUserByID($id){
        if(isset($_GET)){
            $statement = $this->db->prepare('SELECT * FROM User WHERE id_user=:id_user');
            $statement->bindParam(':id_user',$id);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            if($result['id_user']!='')
            {
                $json= array(
                    'error'=>false,
                    'data'=>$result,
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>"No User found with such ID",
                    'status'=>400
                );
                echo json_encode($json);
            }
            
            
        }
    }
    function deleteUserByID($id){
        if(isset($_POST)){
            $statement = $this->db->prepare('DELETE FROM User WHERE id_user=:id_user');
            $statement->bindParam(':id_user',$id);
            $result = $statement->execute();
            if($result == TRUE){
                $json= array(
                    'error'=>false,
                    'data'=>"User is Sucessfully Deleted",
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>"Error Occurred While Deleting User",
                    'status'=>400
                );
                echo json_encode($json);
            }
        }
    }

    function updateUserById($id){
        if(isset($_POST))
        {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $type=$_POST['type'];
            $starting_date = $_POST['starting_date'];
            $address = $_POST['address'];
            $name=$_POST['name'];
            $surname = $_POST['surname'];
            $identity = $_POST['identity'];
            $personal_email=$_POST['personal_email'];
            $primary_phone = $_POST['primary_phone'];
            $secondary_phone=$_POST['secondary_phone'];

            $statement = $this->db->prepare('UPDATE User SET email=:email,password=:password,type=:type,starting_work_date=:starting_date,
                                                                address=:address,name=:name,surname=:surname,identity=:identity,personal_email=:personal_email,
                                                                primary_phone=:primary_phone,secondary_phone=:secondary_phone WHERE id_user=:id_user
                                            ');
            $statement->bindParam(':email',$email);
            $statement->bindParam(':password',md5($password));
            $statement->bindParam(':type',$type);
            $statement->bindParam(':starting_date',$starting_date);
            $statement->bindParam(':address',$address);
            $statement->bindParam(':name',$name);
            $statement->bindParam(':surname',$surname);
            $statement->bindParam(':identity',$identity);
            $statement->bindParam(':personal_email',$personal_email);
            $statement->bindParam(':primary_phone',$primary_phone);
            $statement->bindParam(':secondary_phone',$secondary_phone);
            $statement->bindParam(':id_user',$id);
            $result = $statement->execute();

            if($result == TRUE){
                $json= array(
                    'error'=>false,
                    'data'=>"User is Sucessfully Updated",
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>"Error Occurred While Updating User",
                    'status'=>400
                );
                echo json_encode($json);
            }
                   
        }
    }

    function login()
    {
        require_once('../../config/env.config.php');
        if(isset($_POST))
        {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $statement = $this->db->prepare('SELECT * FROM User WHERE email=:email');
            $statement->bindParam(':email',$email);
            $statement->execute();
            $result=$statement->fetch(PDO::FETCH_ASSOC);
            if($result['email'] != ""){
                if(md5($password) == $result['password'])
                {
                    $payload = array(
                        "iat" => time(),
                        "userId" => $result['id_user'],
                        "userEmail"=>$result['email']
                    );
                    $jwt = JWT::encode($payload, $SECERT_KEY);
                    $json= array(
                        'error'=>false,
                        'data'=>array('user'=>$result,'token'=>$jwt,'msg'=>'You are Successfully Logged In'),
                        'status'=>200
                    );
                    echo json_encode($json);
                }
                else
                {
                    $json= array(
                        'error'=>true,
                        'data'=>array('user'=>array(),'token'=>'','msg'=>'Check your credentials and try-again'),
                        'status'=>400
                    );
                    echo json_encode($json);
                }
            }else{
                $json= array(
                    'error'=>true,
                    'data'=>array('user'=>array(),'token'=>'','msg'=>'Check your credentials and try-again'),
                    'status'=>400
                );
                echo json_encode($json);
            }
        }
        else
        {
            $json= array(
                'error'=>true,
                'data'=>array('token'=>'','msg'=>'BAD REQUEST CHECK REQUEST METHOD'),
                'status'=>400
            );
            echo json_encode($json);
        }
        
    }

    function register(){
        if(isset($_POST))
        {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $type=$_POST['type'];
            $starting_date = $_POST['starting_date'];
            $address = $_POST['address'];
            $name=$_POST['name'];
            $surname = $_POST['surname'];
            $identity = $_POST['identity'];
            $personal_email=$_POST['personal_email'];
            $primary_phone = $_POST['primary_phone'];
            $secondary_phone=$_POST['secondary_phone'];

            $statement = $this->db->prepare('INSERT INTO User (email,password,type,starting_work_date,address,name,surname,identity,personal_email,primary_phone,secondary_phone) 
                                                    VALUES(:email,:password,:type,:starting_date,:address,:name,:surname,:identity,:personal_email,:primary_phone,:secondary_phone)
                                            ');
            $statement->bindParam(':email',$email);
            $statement->bindParam(':password',md5($password));
            $statement->bindParam(':type',$type);
            $statement->bindParam(':starting_date',$starting_date);
            $statement->bindParam(':address',$address);
            $statement->bindParam(':name',$name);
            $statement->bindParam(':surname',$surname);
            $statement->bindParam(':identity',$identity);
            $statement->bindParam(':personal_email',$personal_email);
            $statement->bindParam(':primary_phone',$primary_phone);
            $statement->bindParam(':secondary_phone',$secondary_phone);
            $result = $statement->execute();

            if($result == TRUE){

                $year = date('Y');
                $id_user = $this->db->lastInsertId();
                
                $query = "INSERT INTO Solde (old_solde, taken_days, illness_day, year, id_user)
                          VALUES (2, 0, 0, :year, :id_user)";
                $stm = $this->db->prepare($query);
                $stm->bindParam(':year', $year);
                $stm->bindParam(':id_user', $id_user);
                $stm->execute();
                $json= array(
                    'error'=>false,
                    'data'=>'New record created successfully',
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>false,
                    'data'=>'Error Occurred While Inserting new Record',
                    'status'=>200
                );
                echo json_encode($json);
            }
                   
        }
    }

}

?>