<?php

include_once('./conge.php');
include_once('../../config/database.php');


$con = new DBConnection();
$db = $con->connectDB();
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$conge = new CongeApi($db);

$request_method = $_SERVER['REQUEST_METHOD'];

function handleRequest($request_method, $request, $conge)
    {
        
        
        switch($request_method){
            case 'GET':
                if($request[0]== 'pending'){
                    $id = $request[1];
                    if($id != ""){
                        $conge->getPendingRequestByUserId($id);
                    }else
                    {
                        echo('NO USER FOUND WITH SUCH AN ID');
                    }    
                }
                else if($request[0]== 'request')
                {
                    $id = $request[1];
                    if($id != ""){
                        $conge->getHistoryByUserId($id);
                    }else
                    {
                        echo('NO USER FOUND WITH SUCH AN ID');
                    }    
                }
                break;
            case 'POST':
                if($request[0]== ''){
                    $conge->addHolidayRequest();    
                } else if($request[0]== 'update')
                {
                    $id = $request[1];
                    if($id != ""){
                        $conge->updateHolidayRequest($id);
                    }else
                    {
                        echo('NO USER FOUND WITH SUCH AN ID');
                    }
                } else if($request[0]=='delete'){
                    $id = $request[1];
                    if($id != ""){
                        $conge->deleteHolidayRequestByID($id);
                    }else
                    {
                        echo('NO USER FOUND WITH SUCH AN ID');
                    }
                }
                break;
            
            default:
                break;
        }

    }

    handleRequest($request_method,$request,$conge)
?>