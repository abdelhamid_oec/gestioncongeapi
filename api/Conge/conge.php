<?php
class CongeApi {
    public $db;

    function __construct($db)
    {
        $this->db=$db;
    }

    function getPendingRequestByUserId($id){
        if(isset($_GET)){
            $statement = $this->db->prepare("SELECT * FROM Conge WHERE status='pending' AND id_user=:id_user");
            $statement->bindParam(':id_user',$id);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            $json= array(
                'error'=>false,
                'data'=>$result,
                'status'=>200
            );
            echo json_encode($json);
        }
    }
    function getHistoryByUserId($id){
        if(isset($_GET)){
            $statement = $this->db->prepare("SELECT * FROM Conge WHERE id_user=:id_user");
            $statement->bindParam(':id_user',$id);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            $json= array(
                'error'=>false,
                'data'=>$result,
                'status'=>200
            );
            echo json_encode($json);
        }
    }

    function deleteHolidayRequestByID($id){
        if(isset($_POST)){
            $statement = $this->db->prepare('DELETE FROM Conge WHERE id_conge=:id_conge');
            $statement->bindParam(':id_conge',$id);
            $result = $statement->execute();
            if($result == TRUE){
                $json= array(
                    'error'=>false,
                    'data'=>"Holiday Request is Sucessfully Deleted",
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>"Error Occurred While Deleting Holiday Request",
                    'status'=>400
                );
                echo json_encode($json);
            }
        }
    }


    function updateHolidayRequest($id){
        if(isset($_POST))
        {
            $start_date = $_POST['start_date'];
            $end_date = $_POST['end_date'];
            $holiday_type=$_POST['holiday_type'];
            $description = $_POST['description'];
            $year=$_POST['year'];

            $statement = $this->db->prepare('UPDATE Conge SET start_date=:start_date,end_date=:end_date,holiday_type=:holiday_type,description=:description,year=:year
                                            WHERE id_conge=:id_conge
                                            ');
            $statement->bindParam(':start_date',$start_date);
            $statement->bindParam(':end_date',$end_date);
            $statement->bindParam(':holiday_type',$holiday_type);
            $statement->bindParam(':description',$description);
            $statement->bindParam(':year',$year);
            $statement->bindParam(':id_conge',$id);
            $result = $statement->execute();

            if($result == TRUE){
                //Notification To Be Added Here
                $json= array(
                    'error'=>false,
                    'data'=>'Holiday request Sucessfully Updated',
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>'Error Occurred While Updating a holiday request',
                    'status'=>400
                );
                echo json_encode($json);
            }
                   
        }
    }

    function addHolidayRequest(){
        if(isset($_POST))
        {
            $start_date = $_POST['start_date'];
            $end_date = $_POST['end_date'];
            $holiday_type=$_POST['holiday_type'];
            $description = $_POST['description'];
            $signature = $_POST['signature'];
            $year=date('Y');
            $status="pending";
            $id_user = $_POST['id_user'];
            
            $statement = $this->db->prepare('INSERT INTO Conge (start_date,end_date,holiday_type,description,signature,status,year,id_user) 
                                                    VALUES(:start_date,:end_date,:holiday_type,:description,:signature,:status,:year,:id_user)
                                            ');
            $statement->bindParam(':start_date',$start_date);
            $statement->bindParam(':end_date',$end_date);
            $statement->bindParam(':holiday_type',$holiday_type);
            $statement->bindParam(':description',$description);
            $statement->bindParam(':signature',$signature);
            $statement->bindParam(':status',$status);
            $statement->bindParam(':year',$year);
            $statement->bindParam(':id_user',$id_user);
            $result = $statement->execute();
            
            if($result == TRUE){
                //Notification To Be Added Here
                $json= array(
                    'error'=>false,
                    'data'=>'Holiday request Sucessfully submitted',
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>'Error Occurred While Submitting a holiday request',
                    'status'=>400
                );
                echo json_encode($json);
            }
                   
        }
    }

}

?>