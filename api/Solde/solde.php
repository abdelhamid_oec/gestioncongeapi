<?php
include_once('../models/CommonHoliday.php');
include_once('./config/database.php');
class SoldeApi{
    public $db;

    public function __construct($db)
    {
        $this->db = $db;

    }

   

    public function getAllSolde () {
        if(isset($_GET)){
            $query = "SELECT * 
                      FROM Solde";
            $stm = $this->db->prepare($query);

            $stm->execute();

            $result = $stm->fetchAll(PDO::FETCH_ASSOC);
            $json= array(
                'error'=>false,
                'data'=>$result,
                'status'=>200
            );
            echo json_encode($json);
        }
        
        
        
    }

    public function getSoldeById ($id) {
        if(isset($_GET)){
            $query = "SELECT * 
            FROM Solde
            WHERE id_user = :id_user";
            $stm = $this->db->prepare($query);
            $stm->bindParam(':id_user', $id);

            $stm->execute();

            $result = $stm->fetch(PDO:: FETCH_ASSOC);
            if($result['id_solde']!='')
            {
                $json= array(
                    'error'=>false,
                    'data'=>$result,
                    'status'=>200
                );
                echo json_encode($json);
            }
           
        }else
        {
            $json= array(
                'error'=>true,
                'data'=>"No Solde found with a for user with such an ID",
                'status'=>400
            );
            echo json_encode($json);
        }
        
        
    }

    public function updateSolde ($id) {

        if(isset($_POST)){
            
            $old_solde = $_POST["old_solde"];
            $taken_days = $_POST["taken_days"];
            $illness_day = $_POST["illness_day"];
            $year = $_POST["year"];
            $id_user = $_POST["id_user"];
            
            $query = "UPDATE Solde 
                      SET old_solde = :old_solde, taken_days = :taken_days, illness_day = :illness_day, year = :year, id_user = :id_user
                      WHERE id_solde = :id_solde";
            $stm = $this->db->prepare($query);
            $stm->bindParam(':old_solde', $old_solde);
            $stm->bindParam(':taken_days', $taken_days);
            $stm->bindParam(':illness_day', $illness_day);
            $stm->bindParam(':year', $year);
            $stm->bindParam(':id_user', $id_user);
            $stm->bindParam(':id_solde', $id);

            $result = $stm->execute();
            
            if($result == TRUE){
                $json= array(
                    'error'=>false,
                    'data'=>"Solde is Sucessfully Updated",
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>"Error Occurred While Updating Solde",
                    'status'=>400
                );
                echo json_encode($json);
            }
        }
        
    }

    public function DeleteSolde ($id) {

        if(isset($_POST)){
           
            $query = "DELETE FROM Solde
                      WHERE id_solde = :id_solde";
            $stm = $this->db->prepare($query);
            $stm->bindParam(':id_solde', $id);

            $result = $stm->execute();
            
            if($result == TRUE){
                $json= array(
                    'error'=>false,
                    'data'=>"Solde is Sucessfully Deleted",
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>"Error Occurred While Deleting Solde",
                    'status'=>400
                );
                echo json_encode($json);
            }
        }
    }
}


?>