<?php

include_once('./solde.php');
include_once('../../config/database.php');
//echo($_SERVER['PATH_INFO']);

$con = new DBConnection();
$db = $con->connectDB();
$solde = new SoldeApi($db);

$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$request_method = $_SERVER['REQUEST_METHOD'];

function handleRequest($request_method, $request, $solde)
    {
        
        
        switch($request_method){
            case 'GET':
                if($request[0] == 'getAll'){
                    $solde->getAllSolde();
                }
                elseif($request[0] == 'getById'){
                    $soldeId = $request[1];
                    if($soldeId != '')
                    {
                        $solde->getSoldeById($soldeId);
                    }
                    else{
                        echo "Something went wrong"; 
                    }
                }
                
                break;
            case 'POST':
                if($request[0] == 'update'){
                    $soldeId=$request[1];
                    if($soldeId != "")
                    {
                        $solde->updateSolde($soldeId);
                    }
                    else
                    {
                        echo "Something went wrong";
                    }
                    
                }
                elseif($request[0] == 'delete'){
                    
                    $soldeId = $request[1];
                   
                    if($soldeId != ""){
                        $solde->DeleteSolde($soldeId);
                    }
                    else
                    {
                        echo "Something went wrong";
                    }
                }
                elseif($request[0] == 'insert'){
                   
                    $solde->insertSolde();
                   
                }
                break;
            
            default:
                break;
        }

    }

    handleRequest($request_method,$request,$solde)
?>