<?php
include_once('../models/CommonHoliday.php');
include_once('./config/database.php');
class CommonHolidayApi{
    public $db;

    public function __construct($db)
    {
        $this->db = $db;

    }

    public function insertCommonHoliday (){
        if(isset($_POST)){
            
            
            $year = $_POST["year"];
            $start_date = $_POST["start_date"];
            $end_date = $_POST["end_date"];
            $description = $_POST["description"];
            
            $query = "INSERT INTO common_holiday 
                      (year,start_date,description,end_date)
                      VALUES (:year, :start_date, :description, :end_date)";

            $stm = $this->db->prepare($query);
            $stm->bindParam(':year', $year);
            $stm->bindParam(':start_date', $start_date);
            $stm->bindParam(':end_date', $end_date);
            $stm->bindParam(':description', $description);
            

            $result = $stm->execute();
            if($result == TRUE){
                $json= array(
                    'error'=>false,
                    'data'=>"Common Holiday is Sucessfully Inserted",
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>"Error Occurred While Inserting Common Holiday",
                    'status'=>400
                );
                echo json_encode($json);
            }
        }
    }

    public function getAllCommonHoliday () {
        $query = "SELECT * 
                  FROM common_holiday";
        $stm = $this->db->prepare($query);

        $stm->execute();

        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $json= array(
            'error'=>false,
            'data'=>$result,
            'status'=>200
        );
        echo json_encode($json);
        
    }

    public function getCommonHolidayById ($id) {
        $query = "SELECT * 
                  FROM common_holiday
                  WHERE id_holiday = :id_holiday";
        $stm = $this->db->prepare($query);
        $stm->bindParam(':id_holiday', $id);

        $stm->execute();

        $result = $stm->fetch(PDO:: FETCH_ASSOC);
        if($result['id_holiday']!='')
            {
                $json= array(
                    'error'=>false,
                    'data'=>$result,
                    'status'=>200
                );
                echo json_encode($json);
            }
           
        else
        {
            $json= array(
                'error'=>true,
                'data'=>"No Common Holiday found with such ID",
                'status'=>400
            );
            echo json_encode($json);
        }
    }

    public function updateCommonHoliday ($id) {

        if(isset($_POST)){
            
            $year = $_POST["year"];
            $start_date = $_POST["start_date"];
            $end_date = $_POST["end_date"];
            $description = $_POST["description"];
            
            $query = "UPDATE common_holiday 
                      SET year = :year, start_date = :start_date, end_date = :end_date, description = :description
                      WHERE id_holiday = :id_holiday";
            $stm = $this->db->prepare($query);
            $stm->bindParam(':year', $year);
            $stm->bindParam(':start_date', $start_date);
            $stm->bindParam(':end_date', $end_date);
            $stm->bindParam(':description', $description);
            $stm->bindParam(':id_holiday', $id);

            $result = $stm->execute();
            if($result == TRUE){
                $json= array(
                    'error'=>false,
                    'data'=>"Common Holiday is Sucessfully Updated",
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>"Error Occurred While Updating Common Holiday",
                    'status'=>400
                );
                echo json_encode($json);
            }
        }
        
    }

    public function DeleteCommonHoliday ($id) {

        if(isset($_POST)){
           
            $query = "DELETE FROM common_holiday
                      WHERE id_holiday = :id_holiday";
            $stm = $this->db->prepare($query);
            $stm->bindParam(':id_holiday', $id);

            $result = $stm->execute();
            if($result == TRUE){
                $json= array(
                    'error'=>false,
                    'data'=>"Common Holiday is Sucessfully Deleted",
                    'status'=>200
                );
                echo json_encode($json);
            }
            else
            {
                $json= array(
                    'error'=>true,
                    'data'=>"Error Occurred While Deleting Common Holiday",
                    'status'=>400
                );
                echo json_encode($json);
            }
        }
    }
}


?>