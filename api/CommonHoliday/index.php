<?php

include_once('./commonHoliday.php');
include_once('../../config/database.php');
//echo($_SERVER['PATH_INFO']);

$con = new DBConnection();
$db = $con->connectDB();
$ch = new CommonHolidayApi($db);

$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$request_method = $_SERVER['REQUEST_METHOD'];

function handleRequest($request_method, $request, $ch)
    {
        
        
        switch($request_method){
            case 'GET':
                if($request[0] == 'getAll'){
                    $ch->getAllCommonHoliday();
                }
                elseif($request[0] == 'getById'){
                    $holidayId = $request[1];
                    if($holidayId != '')
                    {
                        $ch->getCommonHolidayById($holidayId);
                    }
                    else{
                        echo "Something went wrong"; 
                    }
                }
                
                break;
            case 'POST':
                if($request[0] == 'update'){
                    $holidayId=$request[1];
                    if($holidayId != "")
                    {
                        $ch->updateCommonHoliday($holidayId);
                    }
                    else
                    {
                        echo "Something went wrong";
                    }
                    
                }
                elseif($request[0] == 'delete'){
                    
                    $holidayId = $request[1];
                    if($holidayId != ""){
                        $ch->DeleteCommonHoliday($holidayId);
                    }
                    else
                    {
                        echo "Something went wrong";
                    }
                }
                elseif($request[0] == 'insert'){
                    $ch->insertCommonHoliday();
                }
                break;
            
            default:
                break;
        }

    }

    handleRequest($request_method,$request,$ch)
?>