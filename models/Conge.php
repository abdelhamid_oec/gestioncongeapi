<?php
class Conge{
    private int $holidayId;
    private DateTime $startDate;
    private DateTime $endDate;
    private string $holidayType;
    private string $description;
    private string $status;
    private string $year;
    private string $signature;
    private int $userId;


    function __construct($holidayId,$startDate,$endDate,$holidayType,$description,$year,$signature,$userId)
    {
        $this->holidayId= $holidayId;
        $this->startDate= $startDate;
        $this->endDate= $endDate;
        $this->holidayType= $holidayType;
        $this->description= $description;
        $this->year= $year;
        $this->signature= $signature;
        $this->userId= $userId;
    }



    function getHolidayId():int{
        return $this->holidayId;
    }

    function setHolidayId($holidayId):void{
        $this->holidayId= $holidayId;
    }

    function getStartDate():DateTime{
        return $this->startDate;
    }

    function setStartDate($startDate):void{
        $this->startDate= $startDate;
    }

    function getEndDate():DateTime{
        return $this->endDate;
    }

    function setEndDate($endDate):void{
        $this->endDate= $endDate;
    }

    function getHolidayType():string{
        return $this->holidayType;
    }

    function setHolidayType($holidayType):void{
        $this->holidayType= $holidayType;
    }

    function getDescription():string{
        return $this->description;
    }

    function setDescription($description):void{
        $this->description= $description;
    }


    function getStatus():string{
        return $this->status;
    }

    function setStatus($status):void{
        $this->status= $status;
    }


    function getYear():string{
        return $this->year;
    }

    function setYear($year):void{
        $this->year= $year;
    }


    function getUserId():int{
        return $this->userId;
    }

    function setUserId($userId):void{
        $this->userId= $userId;
    }


    /**
     * Get the value of signature
     */ 
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set the value of signature
     *
     * @return  self
     */ 
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }
}

?>