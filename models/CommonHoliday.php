<?php
class CommonHoliday{
    private int $id_common_holiday;
    private int $year;
    private DateTime $date;
    private string $description;

    public function __construct($id_common_holiday, $year, $date, $description)
    {
        $this->id_common_holiday = $id_common_holiday;
        $this->year = $year;
        $this->date = $date;
        $this->description = $description;
    }

    


    /**
     * Get the value of id_common_holiday
     */ 
    public function getId_common_holiday()
    {
        return $this->id_common_holiday;
    }

    /**
     * Set the value of id_common_holiday
     *
     * @return  self
     */ 
    public function setId_common_holiday($id_common_holiday)
    {
        $this->id_common_holiday = $id_common_holiday;

        return $this;
    }

    /**
     * Get the value of year
     */ 
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @return  self
     */ 
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
}
?>