<?php
class Notification{
    private int $id_notification;
    private string $text;
    private DateTime $date;
    private bool $status;
    private int $id_user;

    public function __construct($id_notification, $text, $date, $status, $id_user)
    {
        $this->id_notification = $id_notification;
        $this->text = $text;
        $this->date = $date;
        $this->status = $status;
        $this->id_user = $id_user;
    }


    /**
     * Get the value of id_notification
     */ 
    public function getId_notification()
    {
        return $this->id_notification;
    }

    /**
     * Set the value of id_notification
     *
     * @return  self
     */ 
    public function setId_notification($id_notification)
    {
        $this->id_notification = $id_notification;

        return $this;
    }

    /**
     * Get the value of text
     */ 
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set the value of text
     *
     * @return  self
     */ 
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of id_user
     */ 
    public function getId_user()
    {
        return $this->id_user;
    }

    /**
     * Set the value of id_user
     *
     * @return  self
     */ 
    public function setId_user($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }
}
?>