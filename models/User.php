<?php
class User{
    private int $userId;
    private string $email;
    private string $password;
    private string $type;
    private DateTime $starting_work_date;
    private string $address;
    private string $name;
    private string $surname;
    private string $identity;
    private string $personal_email;
    private string $primary_phone;
    private string $secondary_phone;


    function __construct($userId,$email,$password,$type,$starting_work_date,$address,$name,$surname,$identity,$personal_email,$primary_phone,$secondary_phone)
    {
        $this->userId= $userId;
        $this->email= $email;
        $this->password= $password;
        $this->type= $type;
        $this->starting_work_date= $starting_work_date;
        $this->address= $address;
        $this->name= $name;
        $this->surname= $surname;
        $this->identity= $identity;
        $this->personal_email= $personal_email;
        $this->primary_phone= $primary_phone;
        $this->secondary_phone= $secondary_phone;
    }

    function getUserId():int{
        return $this->userId;
    }

    function setUserId($userId):void{
        $this->userId= $userId;
    }

    function getEmail():string{
        return $this->email;
    }

    function setEmail($email):void{
        $this->email= $email;
    }

    function getPassword():string{
        return $this->password;
    }

    function setPassword($password):void{
        $this->password= $password;
    }

    function getType():string{
        return $this->type;
    }

    function setType($type):void{
        $this->type= $type;
    }

    function getStartingWorkDate():DateTime{
        return $this->starting_work_date;
    }

    function setStartingWorkDate($starting_work_date):void{
        $this->starting_work_date= $starting_work_date;
    }

    function getAddress():string{
        return $this->address;
    }

    function setAddress($address):void{
        $this->address= $address;
    }


    function getName():string{
        return $this->name;
    }

    function setName($name):void{
        $this->name= $name;
    }


    function getSurname():string{
        return $this->surname;
    }

    function setSurname($surname):void{
        $this->surname= $surname;
    }

    function getIdentity():string{
        return $this->identity;
    }

    function setIdentity($identity):void{
        $this->identity= $identity;
    }


    function getPersonalEmail():string{
        return $this->personal_email;
    }

    function setPersonalEmail($personal_email):void{
        $this->personal_email= $personal_email;
    }


    function getPrimaryPhone():string{
        return $this->primary_phone;
    }

    function setPrimaryPhone($primary_phone):void{
        $this->primary_phone= $primary_phone;
    }


    function getSecondaryPhone():string{
        return $this->secondary_phone;
    }

    function setSecondaryPhone($secondary_phone):void{
        $this->secondary_phone= $secondary_phone;
    }


}

?>