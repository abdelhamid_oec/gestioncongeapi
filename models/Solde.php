<?php
class Solde{
    private int $id_solde;
    private float $old_solde;
    private float $taken_days;
    private float $illness_days;
    private int $id_user;

    public function __construct($id_solde, $old_solde, $taken_days, $illness_days, $id_user)
    {
        $this->id_solde = $id_solde;
        $this->old_solde = $old_solde;
        $this->taken_days = $taken_days;
        $this->illness_days = $illness_days;
        $this->id_user = $id_user;
    }

    /**
     * Get the value of id_solde
     */ 
    public function getId_solde()
    {
        return $this->id_solde;
    }

    /**
     * Set the value of id_solde
     *
     * @return  self
     */ 
    public function setId_solde($id_solde)
    {
        $this->id_solde = $id_solde;

        return $this;
    }

    /**
     * Get the value of old_solde
     */ 
    public function getOld_solde()
    {
        return $this->old_solde;
    }

    /**
     * Set the value of old_solde
     *
     * @return  self
     */ 
    public function setOld_solde($old_solde)
    {
        $this->old_solde = $old_solde;

        return $this;
    }

    /**
     * Get the value of taken_days
     */ 
    public function getTaken_days()
    {
        return $this->taken_days;
    }

    /**
     * Set the value of taken_days
     *
     * @return  self
     */ 
    public function setTaken_days($taken_days)
    {
        $this->taken_days = $taken_days;

        return $this;
    }

    /**
     * Get the value of illness_days
     */ 
    public function getIllness_days()
    {
        return $this->illness_days;
    }

    /**
     * Set the value of illness_days
     *
     * @return  self
     */ 
    public function setIllness_days($illness_days)
    {
        $this->illness_days = $illness_days;

        return $this;
    }

    /**
     * Get the value of id_user
     */ 
    public function getId_user()
    {
        return $this->id_user;
    }

    /**
     * Set the value of id_user
     *
     * @return  self
     */ 
    public function setId_user($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }
}
?>