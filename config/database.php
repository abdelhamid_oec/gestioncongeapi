<?php
    class DBConnection {
        private $host;
        private $user;
        private $password;
        private $dbname;

        function __construct(){
            $this->host = "localhost";
            $this->user = "root";
            $this->password = "";
            $this->dbname="gestionconge_oec";
        }

        function connectDB(){
            try {
                $con = new PDO("mysql:host=$this->host;dbname=$this->dbname",$this->user,$this->password);
                $con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                //echo "Connection Successfully Established";
                return $con;
            } catch (PDOException $err) {
                //echo "Connection Failed: ". $err->getMessage();
                return null;
            }
        }
    }
?>